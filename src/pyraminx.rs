use core::convert::{TryFrom, TryInto};

use crate::{CycleMove, Puzzle};
use alloc::{format, string::String, vec::Vec};
use Direction::*;
use PositionType::*;

#[derive(Debug)]
pub struct Pyraminx {
    state: Vec<Face>,
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub enum Direction {
    F,
    U,
    L,
    R,
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub enum Face {
    FUL,
    FUR,
    FLR,
    ULR,
}

impl TryFrom<[Direction; 3]> for Face {
    type Error = String;

    fn try_from(mut dirs: [Direction; 3]) -> Result<Self, Self::Error> {
        use Face::*;
        dirs.sort();
        match dirs {
            [F, U, L] => Ok(FUL),
            [F, U, R] => Ok(FUR),
            [F, L, R] => Ok(FLR),
            [U, L, R] => Ok(ULR),
            x => Err(format!("{:?} describes no valid face", x)),
        }
    }
}

#[derive(Debug)]
pub struct Move(Direction, MoveType);

#[derive(Debug)]
pub enum MoveType {
    CW,
    CCW,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Position(PositionType, (Direction, Direction, Direction));

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub enum PositionType {
    Corner,
    Center,
    Edge,
}

impl Position {
    fn face(&self) -> Result<Face, String> {
        let (a, b, c) = self.1.clone();
        [a, b, c].try_into()
    }
}

const PYRAMINX_SOLVED: [Position; 36] = [
    Position(Corner, (F, U, L)),
    Position(Corner, (U, L, F)),
    Position(Corner, (L, F, U)),
    Position(Corner, (F, U, R)),
    Position(Corner, (U, R, F)),
    Position(Corner, (R, F, U)),
    Position(Corner, (F, L, R)),
    Position(Corner, (L, R, F)),
    Position(Corner, (R, F, L)),
    Position(Corner, (U, L, R)),
    Position(Corner, (L, R, U)),
    Position(Corner, (R, U, L)),
    Position(Center, (F, U, L)),
    Position(Center, (U, L, F)),
    Position(Center, (L, F, U)),
    Position(Center, (F, U, R)),
    Position(Center, (U, R, F)),
    Position(Center, (R, F, U)),
    Position(Center, (F, L, R)),
    Position(Center, (L, R, F)),
    Position(Center, (R, F, L)),
    Position(Center, (U, L, R)),
    Position(Center, (L, R, U)),
    Position(Center, (R, U, L)),
    Position(Edge, (F, U, L)),
    Position(Edge, (U, L, F)),
    Position(Edge, (L, F, U)),
    Position(Edge, (F, U, R)),
    Position(Edge, (U, R, F)),
    Position(Edge, (R, F, U)),
    Position(Edge, (F, L, R)),
    Position(Edge, (L, R, F)),
    Position(Edge, (R, F, L)),
    Position(Edge, (U, L, R)),
    Position(Edge, (L, R, U)),
    Position(Edge, (R, U, L)),
];

impl Puzzle for Pyraminx {
    type Face = Face;
    type Position = Position;
    type Move = Move;
    type Error = String;

    fn permute(&mut self, a: &Self::Position, b: &Self::Position) -> Result<(), Self::Error> {
        let ia = PYRAMINX_SOLVED
            .iter()
            .position(|x| x == a)
            .ok_or(format!("{:?} not valid", a))?;
        let ib = PYRAMINX_SOLVED
            .iter()
            .position(|x| x == b)
            .ok_or(format!("{:?} not valid", b))?;
        self.state.swap(ia, ib);
        Ok(())
    }

    fn face(&self, p: &Self::Position) -> Result<&Self::Face, Self::Error> {
        let i = PYRAMINX_SOLVED
            .iter()
            .position(|x| x == p)
            .ok_or(format!("{:?} not valid", p))?;
        Ok(&self.state[i])
    }

    fn solved(&self) -> bool {
        (0..PYRAMINX_SOLVED.len()).all(|x| Ok(&self.state[x]) == PYRAMINX_SOLVED[x].face().as_ref())
    }
}

impl CycleMove<Position> for Move {
    fn cycles(&self) -> Vec<Vec<Position>> {
        Vec::new()
    }
}
