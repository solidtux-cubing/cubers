#[cfg(feature = "color")]
use crate::cube::face_to_color;
use crate::{
    cube::{
        CubeFace::{self, *},
        MoveType,
    },
    CycleMove, Puzzle,
};
use alloc::{
    fmt::{Display, Formatter},
    format,
    str::FromStr,
    string::{String, ToString},
    vec,
    vec::Vec,
};
#[cfg(feature = "rand")]
use rand::prelude::*;

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub struct CubeMove2(CubeFace, MoveType);

impl Display for CubeMove2 {
    fn fmt(&self, f: &mut Formatter) -> alloc::fmt::Result {
        write!(f, "{}{}", self.0, self.1)
    }
}

#[cfg(feature = "rand")]
pub fn random_moves2<R: Rng>(n: usize, rng: &mut R) -> Vec<CubeMove2> {
    let mut res = Vec::new();
    let mut last = None;
    while res.len() < n {
        let f = [U, D, L, R, F, B].choose(rng).unwrap().clone();
        let t = [MoveType::CW, MoveType::CCW, MoveType::Double]
            .choose(rng)
            .unwrap()
            .clone();
        if let Some(ref l) = last {
            if f == *l {
                continue;
            }
        }
        last = Some(f.clone());
        res.push(CubeMove2(f, t));
    }
    res
}

pub type Position2 = (CubeFace, CubeFace, CubeFace);

#[derive(Debug, Clone)]
pub struct Cube2 {
    state: Vec<CubeFace>,
}

const CUBE2_SOLVED: [Position2; 24] = [
    (U, F, L),
    (U, F, R),
    (U, B, L),
    (U, B, R),
    (D, F, L),
    (D, F, R),
    (D, B, L),
    (D, B, R),
    (F, U, L),
    (F, U, R),
    (F, D, L),
    (F, D, R),
    (B, U, L),
    (B, U, R),
    (B, D, L),
    (B, D, R),
    (L, U, B),
    (L, U, F),
    (L, D, B),
    (L, D, F),
    (R, U, B),
    (R, U, F),
    (R, D, B),
    (R, D, F),
];

impl Cube2 {
    #[cfg(feature = "color")]
    pub fn print(&self) -> Result<(), String> {
        println!(
            "    {} {}",
            face_to_color(self.face(&(U, B, L))?),
            face_to_color(self.face(&(U, B, R))?),
        );
        println!(
            "    {} {}",
            face_to_color(self.face(&(U, F, L))?),
            face_to_color(self.face(&(U, F, R))?),
        );
        println!(
            "{} {} {} {} {} {} {} {}",
            face_to_color(self.face(&(L, U, B))?),
            face_to_color(self.face(&(L, U, F))?),
            face_to_color(self.face(&(F, U, L))?),
            face_to_color(self.face(&(F, U, R))?),
            face_to_color(self.face(&(R, U, F))?),
            face_to_color(self.face(&(R, U, B))?),
            face_to_color(self.face(&(B, U, R))?),
            face_to_color(self.face(&(B, U, L))?),
        );
        println!(
            "{} {} {} {} {} {} {} {}",
            face_to_color(self.face(&(L, D, B))?),
            face_to_color(self.face(&(L, D, F))?),
            face_to_color(self.face(&(F, D, L))?),
            face_to_color(self.face(&(F, D, R))?),
            face_to_color(self.face(&(R, D, F))?),
            face_to_color(self.face(&(R, D, B))?),
            face_to_color(self.face(&(B, D, R))?),
            face_to_color(self.face(&(B, D, L))?),
        );
        println!(
            "    {} {}",
            face_to_color(self.face(&(D, F, L))?),
            face_to_color(self.face(&(D, F, R))?),
        );
        println!(
            "    {} {}",
            face_to_color(self.face(&(D, B, L))?),
            face_to_color(self.face(&(D, B, R))?),
        );
        Ok(())
    }
}

impl Default for Cube2 {
    fn default() -> Self {
        Cube2 {
            state: CUBE2_SOLVED.iter().cloned().map(|x| x.0).collect(),
        }
    }
}

impl Puzzle for Cube2 {
    type Face = CubeFace;
    type Position = Position2;
    type Move = CubeMove2;
    type Error = String;

    //TODO better errors
    fn permute(&mut self, a: &Self::Position, b: &Self::Position) -> Result<(), Self::Error> {
        let ia = CUBE2_SOLVED
            .iter()
            .position(|x| x == a)
            .ok_or(format!("{:?} not valid", a))?;
        let ib = CUBE2_SOLVED
            .iter()
            .position(|x| x == b)
            .ok_or(format!("{:?} not valid", b))?;
        self.state.swap(ia, ib);
        Ok(())
    }

    fn face(&self, p: &Self::Position) -> Result<&Self::Face, Self::Error> {
        let i = CUBE2_SOLVED
            .iter()
            .position(|x| x == p)
            .ok_or(format!("{:?} not valid", p))?;
        Ok(&self.state[i])
    }

    fn solved(&self) -> bool {
        (0..CUBE2_SOLVED.len()).all(|x| self.state[x] == CUBE2_SOLVED[x].0)
    }
}

impl FromStr for CubeMove2 {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() == 0 {
            return Err("empty string received".to_string());
        }
        let f = match &s[0..1] {
            "U" => U,
            "D" => D,
            "L" => L,
            "R" => R,
            "F" => F,
            "B" => B,
            x => return Err(format!("{} is no valid face", x)),
        };
        let t = match &s[1..s.len()] {
            "" => MoveType::CW,
            "'" => MoveType::CCW,
            "2" => MoveType::Double,
            x => return Err(format!("{} is no valid move type", x)),
        };
        Ok(CubeMove2(f, t))
    }
}

impl CycleMove<Position2> for CubeMove2 {
    fn cycles(&self) -> Vec<Vec<Position2>> {
        let res = match &self.0 {
            U => vec![
                vec![(U, F, R), (U, F, L), (U, B, L), (U, B, R)],
                vec![(F, U, L), (L, U, B), (B, U, R), (R, U, F)],
                vec![(F, U, R), (L, U, F), (B, U, L), (R, U, B)],
            ],
            D => vec![
                vec![(D, F, L), (D, F, R), (D, B, R), (D, B, L)],
                vec![(F, D, L), (R, D, F), (B, D, R), (L, D, B)],
                vec![(F, D, R), (R, D, B), (B, D, L), (L, D, F)],
            ],
            L => vec![
                vec![(L, U, B), (L, U, F), (L, D, F), (L, D, B)],
                vec![(F, U, L), (D, F, L), (B, D, L), (U, B, L)],
                vec![(F, D, L), (D, B, L), (B, U, L), (U, F, L)],
            ],
            R => vec![
                vec![(R, U, F), (R, U, B), (R, D, B), (R, D, F)],
                vec![(F, U, R), (U, B, R), (B, D, R), (D, F, R)],
                vec![(F, D, R), (U, F, R), (B, U, R), (D, B, R)],
            ],
            F => vec![
                vec![(F, U, L), (F, U, R), (F, D, R), (F, D, L)],
                vec![(L, U, F), (U, F, R), (R, D, F), (D, F, L)],
                vec![(L, D, F), (U, F, L), (R, U, F), (D, F, R)],
            ],
            B => vec![
                vec![(B, U, R), (B, U, L), (B, D, L), (B, D, R)],
                vec![(U, B, R), (L, U, B), (D, B, L), (R, D, B)],
                vec![(U, B, L), (L, D, B), (D, B, R), (R, U, B)],
            ],
        };
        match &self.1 {
            MoveType::CW => res,
            MoveType::CCW => [&res[..], &res[..], &res[..]].concat(),
            MoveType::Double => [&res[..], &res[..]].concat(),
        }
    }
}

#[test]
fn cube2_default() {
    let c = Cube2::default();
    assert_eq!(c.state.len(), 6 * 4);
    assert!(c.solved());
}
