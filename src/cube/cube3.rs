use self::Position3::*;
#[cfg(feature = "color")]
use crate::cube::face_to_color;
use crate::{
    cube::{
        CubeFace::{self, *},
        MoveType,
    },
    CycleMove, Puzzle,
};
use alloc::{
    fmt::{Display, Formatter},
    format,
    str::FromStr,
    string::{String, ToString},
    vec,
    vec::Vec,
};
#[cfg(feature = "rand")]
use rand::prelude::*;

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub struct CubeMove3(CubeFace, MoveType);

impl Display for CubeMove3 {
    fn fmt(&self, f: &mut Formatter) -> alloc::fmt::Result {
        write!(f, "{}{}", self.0, self.1)
    }
}

#[cfg(feature = "rand")]
pub fn random_moves3<R: Rng>(n: usize, rng: &mut R) -> Vec<CubeMove3> {
    let mut res = Vec::new();
    let mut last = None;
    while res.len() < n {
        let f = [U, D, L, R, F, B].choose(rng).unwrap().clone();
        let t = [MoveType::CW, MoveType::CCW, MoveType::Double]
            .choose(rng)
            .unwrap()
            .clone();
        if let Some(ref l) = last {
            if f == *l {
                continue;
            }
        }
        last = Some(f.clone());
        res.push(CubeMove3(f, t));
    }
    res
}

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub enum Position3 {
    Corner(CubeFace, CubeFace, CubeFace),
    Edge(CubeFace, CubeFace),
}

impl Position3 {
    fn dir(&self) -> CubeFace {
        match self {
            Corner(x, _, _) => x.clone(),
            Edge(x, _) => x.clone(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Cube3 {
    state: Vec<CubeFace>,
}

const CUBE3_SOLVED: [Position3; 48] = [
    Corner(U, F, L),
    Corner(U, F, R),
    Corner(U, B, L),
    Corner(U, B, R),
    Corner(D, F, L),
    Corner(D, F, R),
    Corner(D, B, L),
    Corner(D, B, R),
    Corner(F, U, L),
    Corner(F, U, R),
    Corner(F, D, L),
    Corner(F, D, R),
    Corner(B, U, L),
    Corner(B, U, R),
    Corner(B, D, L),
    Corner(B, D, R),
    Corner(L, U, B),
    Corner(L, U, F),
    Corner(L, D, B),
    Corner(L, D, F),
    Corner(R, U, B),
    Corner(R, U, F),
    Corner(R, D, B),
    Corner(R, D, F),
    Edge(U, F),
    Edge(U, B),
    Edge(U, L),
    Edge(U, R),
    Edge(D, F),
    Edge(D, B),
    Edge(D, L),
    Edge(D, R),
    Edge(L, U),
    Edge(L, D),
    Edge(L, F),
    Edge(L, B),
    Edge(R, U),
    Edge(R, D),
    Edge(R, F),
    Edge(R, B),
    Edge(F, U),
    Edge(F, D),
    Edge(F, L),
    Edge(F, R),
    Edge(B, U),
    Edge(B, D),
    Edge(B, L),
    Edge(B, R),
];

impl Cube3 {
    #[cfg(feature = "color")]
    pub fn print(&self) -> Result<(), String> {
        println!(
            "      {} {} {}",
            face_to_color(self.face(&Corner(U, B, L))?),
            face_to_color(self.face(&Edge(U, B))?),
            face_to_color(self.face(&Corner(U, B, R))?),
        );
        println!(
            "      {} {} {}",
            face_to_color(self.face(&Edge(U, L))?),
            face_to_color(&U),
            face_to_color(self.face(&Edge(U, R))?),
        );
        println!(
            "      {} {} {}",
            face_to_color(self.face(&Corner(U, F, L))?),
            face_to_color(self.face(&Edge(U, F))?),
            face_to_color(self.face(&Corner(U, F, R))?),
        );
        println!(
            "{} {} {} {} {} {} {} {} {} {} {} {}",
            face_to_color(self.face(&Corner(L, U, B))?),
            face_to_color(self.face(&Edge(L, U))?),
            face_to_color(self.face(&Corner(L, U, F))?),
            face_to_color(self.face(&Corner(F, U, L))?),
            face_to_color(self.face(&Edge(F, U))?),
            face_to_color(self.face(&Corner(F, U, R))?),
            face_to_color(self.face(&Corner(R, U, F))?),
            face_to_color(self.face(&Edge(R, U))?),
            face_to_color(self.face(&Corner(R, U, B))?),
            face_to_color(self.face(&Corner(B, U, R))?),
            face_to_color(self.face(&Edge(B, U))?),
            face_to_color(self.face(&Corner(B, U, L))?),
        );
        println!(
            "{} {} {} {} {} {} {} {} {} {} {} {}",
            face_to_color(self.face(&Edge(L, B))?),
            face_to_color(&L),
            face_to_color(self.face(&Edge(L, F))?),
            face_to_color(self.face(&Edge(F, L))?),
            face_to_color(&F),
            face_to_color(self.face(&Edge(F, R))?),
            face_to_color(self.face(&Edge(R, F))?),
            face_to_color(&R),
            face_to_color(self.face(&Edge(R, B))?),
            face_to_color(self.face(&Edge(B, R))?),
            face_to_color(&B),
            face_to_color(self.face(&Edge(B, L))?),
        );
        println!(
            "{} {} {} {} {} {} {} {} {} {} {} {}",
            face_to_color(self.face(&Corner(L, D, B))?),
            face_to_color(self.face(&Edge(L, D))?),
            face_to_color(self.face(&Corner(L, D, F))?),
            face_to_color(self.face(&Corner(F, D, L))?),
            face_to_color(self.face(&Edge(F, D))?),
            face_to_color(self.face(&Corner(F, D, R))?),
            face_to_color(self.face(&Corner(R, D, F))?),
            face_to_color(self.face(&Edge(R, D))?),
            face_to_color(self.face(&Corner(R, D, B))?),
            face_to_color(self.face(&Corner(B, D, R))?),
            face_to_color(self.face(&Edge(B, D))?),
            face_to_color(self.face(&Corner(B, D, L))?),
        );
        println!(
            "      {} {} {}",
            face_to_color(self.face(&Corner(D, F, L))?),
            face_to_color(self.face(&Edge(D, F))?),
            face_to_color(self.face(&Corner(D, F, R))?),
        );
        println!(
            "      {} {} {}",
            face_to_color(self.face(&Edge(D, L))?),
            face_to_color(&D),
            face_to_color(self.face(&Edge(D, R))?),
        );
        println!(
            "      {} {} {}",
            face_to_color(self.face(&Corner(D, B, L))?),
            face_to_color(self.face(&Edge(D, B))?),
            face_to_color(self.face(&Corner(D, B, R))?),
        );
        Ok(())
    }
}

impl Default for Cube3 {
    fn default() -> Self {
        Cube3 {
            state: CUBE3_SOLVED.iter().cloned().map(|x| x.dir()).collect(),
        }
    }
}

impl Puzzle for Cube3 {
    type Face = CubeFace;
    type Position = Position3;
    type Move = CubeMove3;
    type Error = String;

    //TODO better errors
    fn permute(&mut self, a: &Self::Position, b: &Self::Position) -> Result<(), Self::Error> {
        let ia = CUBE3_SOLVED
            .iter()
            .position(|x| x == a)
            .ok_or(format!("{:?} not valid", a))?;
        let ib = CUBE3_SOLVED
            .iter()
            .position(|x| x == b)
            .ok_or(format!("{:?} not valid", b))?;
        self.state.swap(ia, ib);
        Ok(())
    }

    fn face(&self, p: &Self::Position) -> Result<&Self::Face, Self::Error> {
        let i = CUBE3_SOLVED
            .iter()
            .position(|x| x == p)
            .ok_or(format!("{:?} not valid", p))?;
        Ok(&self.state[i])
    }

    fn solved(&self) -> bool {
        (0..CUBE3_SOLVED.len()).all(|x| self.state[x] == CUBE3_SOLVED[x].dir())
    }
}

impl FromStr for CubeMove3 {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() == 0 {
            return Err("empty string received".to_string());
        }
        let f = s[0..1].parse()?;
        let t = match &s[1..s.len()] {
            "" => MoveType::CW,
            "'" => MoveType::CCW,
            "2" => MoveType::Double,
            x => return Err(format!("{} is no valid move type", x)),
        };
        Ok(CubeMove3(f, t))
    }
}

impl CycleMove<Position3> for CubeMove3 {
    fn cycles(&self) -> Vec<Vec<Position3>> {
        let res = match &self.0 {
            U => vec![
                vec![
                    Corner(U, F, R),
                    Corner(U, F, L),
                    Corner(U, B, L),
                    Corner(U, B, R),
                ],
                vec![
                    Corner(F, U, L),
                    Corner(L, U, B),
                    Corner(B, U, R),
                    Corner(R, U, F),
                ],
                vec![
                    Corner(F, U, R),
                    Corner(L, U, F),
                    Corner(B, U, L),
                    Corner(R, U, B),
                ],
                vec![Edge(U, F), Edge(U, L), Edge(U, B), Edge(U, R)],
                vec![Edge(F, U), Edge(L, U), Edge(B, U), Edge(R, U)],
            ],
            D => vec![
                vec![
                    Corner(D, F, L),
                    Corner(D, F, R),
                    Corner(D, B, R),
                    Corner(D, B, L),
                ],
                vec![
                    Corner(F, D, L),
                    Corner(R, D, F),
                    Corner(B, D, R),
                    Corner(L, D, B),
                ],
                vec![
                    Corner(F, D, R),
                    Corner(R, D, B),
                    Corner(B, D, L),
                    Corner(L, D, F),
                ],
                vec![Edge(D, F), Edge(D, R), Edge(D, B), Edge(D, L)],
                vec![Edge(F, D), Edge(R, D), Edge(B, D), Edge(L, D)],
            ],
            L => vec![
                vec![
                    Corner(L, U, B),
                    Corner(L, U, F),
                    Corner(L, D, F),
                    Corner(L, D, B),
                ],
                vec![
                    Corner(F, U, L),
                    Corner(D, F, L),
                    Corner(B, D, L),
                    Corner(U, B, L),
                ],
                vec![
                    Corner(F, D, L),
                    Corner(D, B, L),
                    Corner(B, U, L),
                    Corner(U, F, L),
                ],
                vec![Edge(L, U), Edge(L, F), Edge(L, D), Edge(L, B)],
                vec![Edge(U, L), Edge(F, L), Edge(D, L), Edge(B, L)],
            ],
            R => vec![
                vec![
                    Corner(R, U, F),
                    Corner(R, U, B),
                    Corner(R, D, B),
                    Corner(R, D, F),
                ],
                vec![
                    Corner(F, U, R),
                    Corner(U, B, R),
                    Corner(B, D, R),
                    Corner(D, F, R),
                ],
                vec![
                    Corner(F, D, R),
                    Corner(U, F, R),
                    Corner(B, U, R),
                    Corner(D, B, R),
                ],
                vec![Edge(R, U), Edge(R, B), Edge(R, D), Edge(R, F)],
                vec![Edge(U, R), Edge(B, R), Edge(D, R), Edge(F, R)],
            ],
            F => vec![
                vec![
                    Corner(F, U, L),
                    Corner(F, U, R),
                    Corner(F, D, R),
                    Corner(F, D, L),
                ],
                vec![
                    Corner(L, U, F),
                    Corner(U, F, R),
                    Corner(R, D, F),
                    Corner(D, F, L),
                ],
                vec![
                    Corner(L, D, F),
                    Corner(U, F, L),
                    Corner(R, U, F),
                    Corner(D, F, R),
                ],
                vec![Edge(F, U), Edge(F, R), Edge(F, D), Edge(F, L)],
                vec![Edge(U, F), Edge(R, F), Edge(D, F), Edge(L, F)],
            ],
            B => vec![
                vec![
                    Corner(B, U, R),
                    Corner(B, U, L),
                    Corner(B, D, L),
                    Corner(B, D, R),
                ],
                vec![
                    Corner(U, B, R),
                    Corner(L, U, B),
                    Corner(D, B, L),
                    Corner(R, D, B),
                ],
                vec![
                    Corner(U, B, L),
                    Corner(L, D, B),
                    Corner(D, B, R),
                    Corner(R, U, B),
                ],
                vec![Edge(B, U), Edge(B, L), Edge(B, D), Edge(B, R)],
                vec![Edge(U, B), Edge(L, B), Edge(D, B), Edge(R, B)],
            ],
        };
        match &self.1 {
            MoveType::CW => res,
            MoveType::CCW => [&res[..], &res[..], &res[..]].concat(),
            MoveType::Double => [&res[..], &res[..]].concat(),
        }
    }
}

#[test]
fn cube3_default() {
    let c = Cube3::default();
    assert_eq!(c.state.len(), 6 * 4 + 6 * 4);
    assert!(c.solved());
}
